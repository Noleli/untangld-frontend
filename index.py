#!/usr/bin/python

execfile('site.py')
#import mapper
import simplejson as json
import sys
import cgi

# if the mapper worked, we'd probably have objects like this
class entity:
    def __init__(self, id, name, kind):
        self.id = id
        self.name = name
        self.kind = kind

people = [entity(23, "Bradley A. Stephens", "PERSON"),
    entity(22, "Donald Stephens", "PERSON"),
    entity(26, "B.J. Nickell", "PERSON"),
    entity(27, "James Whitley", "PERSON"),
    entity(28, "Barbara Jean Nickell Stephens", "PERSON"),
    entity(31, "Suzanna Stephens", "PERSON"),
    entity(34, "Noah Liebman", "PERSON")]
places = [entity(25, "1503 Collage Av, Quincey, IL 62301", "PLACE")]

print "Content-type: text/html\n\n"

print """
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <div class="container">
        <div class="row">
            <div class="span4"><h1>Entities</h1></div>
        </div>"""
for i, e in enumerate(people):
    if i % 6 == 0:
        print('<div class="row">')
    print('\t<div class="span2"><div class="entity_container"><div id="person-' + str(e.id) + '" class="entity person"></div><p>' + str(e.name) + '</p></div></div>')
    if i % 6 == 5 or i == len(people) - 1:
        print('</div>')
print """
        <br>
        <div class="row">
            <div class="span3"><div id="place-1" class="entity place"></div></div>
            <div class="span3"><div id="place-2" class="entity place"></div></div>
            <div class="span3"><div id="place-3" class="entity place"></div></div>
            <div class="span3"><div id="place-4" class="entity place"></div></div>
        </div>
    </div><!-- /container-->
    
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
"""