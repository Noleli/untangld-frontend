$(document).ready(init);

function init()
{
	// replace this with an ajax call to download all the data objects
	var people_links = {"person-1": ["place-3", "place-1"], "person-2": ["place-2"], "person-3": ["place-1", "place-4"]};
	var place_links = {"place-1": ["person-1"], "place-2": ["person-2"]};
	
	var people_obj = $("div.person");
	var places_obj = $("div.place");
	
	people_obj.each(
		function()
		{
			$(this).hover(
				function()
				{
					$(this).addClass("hover");
					curId = $(this).attr("id");
					for(i in people_links[curId])
					{
						$("#" + people_links[curId][i]).addClass("hover");
					}
				},
				function()
				{
					$(this).removeClass("hover");
					curId = $(this).attr("id");
					for(i in people_links[curId])
					{
						$("#" + people_links[curId][i]).removeClass("hover");
					}
				}
			);
		}
	);
}